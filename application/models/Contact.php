<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Contact extends CI_Model {
  function __construct()
  {
    parent::__construct();
  }
  
  function getall() 
  {
    $data=$this->db->get_where('contacts',array('owner'=>$this->session->userdata('user')->email));
    return $data->result();
  }
  
  function set($data)
  {
    $data->owner=$this->session->userdata('user')->email;
    if($this->get($data->id)==false)
      return $this->db->insert('contacts',$data);
    else
      return $this->db->update('contacts',$data,array('owner'=>$this->session->userdata('user')->email,'id'=>$data->id));
  }

  function get($id) 
  {
    $data=$this->db->get_where('contacts',array('owner'=>$this->session->userdata('user')->email,'id'=>$id));
    return $data->row(0);
  }

  function del($id) 
  {
    return $this->db->delete('contacts',array('owner'=>$this->session->userdata('user')->email,'id'=>$id));
  }
}
?>
