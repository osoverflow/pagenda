<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User extends CI_Model {
  function __construct()
  {
    parent::__construct();
  }

  function get($email)
  {
    $data=$this->db->get_where('users',array('email'=>$email));
    if($data->num_rows()<=0) return false;
    return $data->row(0);
  }
  
  function set($data)
  {
    return $this->db->insert('users',$data);
  }
}
?>
