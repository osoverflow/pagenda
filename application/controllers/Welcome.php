<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {
  var $parts;
  public function __construct()
  {
    parent::__construct();
    $this->load->database();

  }
	public function index()
	{
    $this->parts['header']=$this->load->view('header','',true);
    $this->parts['footer']=$this->load->view('footer','',true);
    if(en_sesion())
    {
      $elementos=$this->Contact->getall();
      $this->parts['body']=$this->load->view('listado',compact('elementos'),true);
    }
    else
      $this->parts['body']=$this->load->view('home','',true);
		$this->load->view('welcome_message', $this->parts);
	}
  
  public function registro()
  {
    $this->parts['header']=$this->load->view('header','',true);
    $this->parts['footer']=$this->load->view('footer','',true);
    if($this->input->post())
    {
      $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
      $this->form_validation->set_rules('password', 'Contraseña', 'trim|required');
      if ($this->form_validation->run() == FALSE)
        $this->parts['body']=$this->load->view('registro','',true);
      else
      {
        $email=$this->User->get($this->input->post('email'));
        if($email==false) 
        {
          $this->User->set((object)array('email'=>$this->input->post('email'),'password'=>$this->input->post('password')));
          $this->parts['body']=$this->load->view('registrado','',true);
        }
        else
        {
          $this->parts['body']=$this->load->view('registro',array('yaexiste'=>1),true);
        }
      }
    }
    else
    {
      $this->parts['body']=$this->load->view('registro','',true);
    }
		$this->load->view('welcome_message', $this->parts);
  }
  
  public function login()
  {
    $this->parts['header']=$this->load->view('header','',true);
    $this->parts['footer']=$this->load->view('footer','',true);
    if($this->input->post())
    {
      $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
      $this->form_validation->set_rules('password', 'Contraseña', 'trim|required');
      if ($this->form_validation->run() == FALSE)
        $this->parts['body']=$this->load->view('loginerror','',true);
      else
      {
        $user=$this->User->get($this->input->post('email'));
        if($user!==false && $user->password==$this->input->post('password')) 
        {
          $this->session->set_userdata("user",$user);
          redirect("/");
        }
        else
          $this->parts['body']=$this->load->view('loginerror','',true);
      }
    }
    else
    {
      $this->parts['body']=$this->load->view('loginerror','',true);
    }
		$this->load->view('welcome_message', $this->parts);
  }

  public function logout()
  {
    $this->session->unset_userdata('user');
    redirect("/");
  }
  
  public function agrega()
  {
    $this->parts['header']=$this->load->view('header','',true);
    $this->parts['footer']=$this->load->view('footer','',true);
    if($this->input->post())
    {
      $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
      $this->form_validation->set_rules('nombre', 'Nombre', 'trim|required');
      $this->form_validation->set_rules('telefono', 'Teléfono', 'trim|required');
      $this->form_validation->set_rules('direccion', 'Dirección', 'trim');
      if ($this->form_validation->run() == FALSE)
        $this->parts['body']=$this->load->view('agrega','',true);
      else
      {
        $contacto=new stdClass;
        $contacto->id=md5(uniqid());
        $contacto->email=$this->input->post('email');
        $contacto->nombre=$this->input->post('nombre');
        $contacto->telefono=$this->input->post('telefono');
        $contacto->direccion=$this->input->post('direccion');
        $this->Contact->set($contacto);
        redirect("/");
      }
    }
    else
    {
      $this->parts['body']=$this->load->view('agrega','',true);
    }
		$this->load->view('welcome_message', $this->parts);
  }
  
  public function modifica($id=null)
  {
    $this->parts['header']=$this->load->view('header','',true);
    $this->parts['footer']=$this->load->view('footer','',true);
    if($this->input->post())
    {
      $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
      $this->form_validation->set_rules('nombre', 'Nombre', 'trim|required');
      $this->form_validation->set_rules('telefono', 'Teléfono', 'trim|required');
      $this->form_validation->set_rules('direccion', 'Dirección', 'trim');
      if ($this->form_validation->run() == FALSE)
        $this->parts['body']=$this->load->view('agrega','',true);
      else
      {
        $contacto=new stdClass;
        $contacto->id=$this->input->post('id');
        $contacto->email=$this->input->post('email');
        $contacto->nombre=$this->input->post('nombre');
        $contacto->telefono=$this->input->post('telefono');
        $contacto->direccion=$this->input->post('direccion');
        $this->Contact->set($contacto);
        redirect("/");
      }
    }
    else
    {
      $contacto=$this->Contact->get($id);
      $this->parts['body']=$this->load->view('agrega',compact('contacto'),true);
    }
		$this->load->view('welcome_message', $this->parts);
  }
  
  public function borra($id=null)
  {
    $this->Contact->del($id);
    redirect("/");
  }
  
}
