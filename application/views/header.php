    <nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Conmutar navegacion</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="<?=site_url("/")?>">PHP Agenda</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <?php if(!en_sesion()) { ?>
          <form class="navbar-form navbar-right" method="POST" action="<?=site_url("Welcome/login")?>">
            <div class="form-group">
              <input type="text" placeholder="Email" class="form-control" name="email">
            </div>
            <div class="form-group">
              <input type="password" placeholder="Contraseña" class="form-control" name="password">
            </div>
            <button type="submit" class="btn btn-success">Acceder</button>
            <a href="<?=site_url("Welcome/registro")?>"  class="btn btn-info">Registrarme</a>
            
          </form>
          <?php } else { ?>
            <div class="navbar-form navbar-right">
              <a href="<?=site_url("Welcome/logout")?>"  class="btn btn-info">Salir</a>
            </div>
          <?php } ?>
        </div>
      </div>
    </nav>
