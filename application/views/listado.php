      <div class="table-responsive col-xs-12 col-md-8 col-md-offset-2">
        <table class="table table-striped table-hover">
          <thead>
            <tr>
              <th class="col-md-2">Nombre</th>
              <th class="col-md-2">Teléfono</th>
              <th class="col-md-2">eMail</th>
              <th class="col-md-5">Dirección</th>
              <th class="col-md-1"><a href="<?=site_url("/Welcome/agrega")?>"><i class="glyphicon glyphicon-plus green"></i></a></th>
            </tr>
          </thead>
          <tbody>
            <?php if(isset($elementos) && is_array($elementos)) foreach($elementos as $e) { ?>
            <tr>
              <td><?=$e->nombre?></td>
              <td><?=$e->telefono?></td>
              <td><?=$e->email?></td>
              <td><?=$e->direccion?></td>
              <td><a href="<?=site_url("/Welcome/borra/".$e->id)?>"><i class="glyphicon glyphicon-trash"></i></a>
              <a href="<?=site_url("/Welcome/modifica/".$e->id)?>"><i class="glyphicon glyphicon-pencil"></i></a></td>
            </tr>
            <?php } ?>
          </tbody>
        </table>
      </div>
