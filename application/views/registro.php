      <div class="col-xs-6 col-md-4 col-md-offset-4 col-xs-offset-3">
        <?php echo validation_errors('<div class="alert alert-danger">','</div>'); ?>
        <?php if(isset($yaexiste)) echo '<div class="alert alert-danger">Ya existe el usuario</div>'; ?>
        <form class="form" method="POST" action="<?=site_url("Welcome/registro")?>">
          <div class="form-group">
            <label for="email">Dirección de e-Mail</label>
            <div class="input-group">
              <span class="input-group-addon" id="email-addon">@</span>
              <input type="text" class="form-control" placeholder="Email" aria-describedby="email-addon" id="email" name="email" value="<?php echo set_value('email'); ?>">
            </div>
          </div>
          <div class="form-group">
            <label for="password">Contraseña</label>
            <div class="input-group">
              <span class="input-group-addon" id="password-addon">*</span>
              <input type="text" class="form-control" placeholder="Contraseña" aria-describedby="password-addon" id="password" name="password" value="<?php echo set_value('password'); ?>">
            </div>
          </div>
          <div class="form-group">
            <input type="submit" class="form-control btn btn-success" value="Registrarme">
          </div>
        </form>
      </div>
