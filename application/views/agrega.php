      <div class="col-xs-6 col-md-4 col-md-offset-4 col-xs-offset-3">
        <?php echo validation_errors('<div class="alert alert-danger">','</div>'); ?>
        <form class="form" method="POST" action="<?=isset($contacto)&&isset($contacto->id)?site_url("Welcome/modifica"):site_url("Welcome/agrega")?>">
          <div class="form-group">
            <label for="nombre">Nombre</label>
            <div class="input-group">
              <span class="input-group-addon" id="nombre-addon"><i class="glyphicon glyphicon-user"></i></span>
              <input type="text" class="form-control" placeholder="Nombre" aria-describedby="nombre-addon" id="nombre" name="nombre" value="<?php echo set_value('nombre', isset($contacto->nombre)?$contacto->nombre:''); ?>">
            </div>
          </div>
          <div class="form-group">
            <label for="telefono">Teléfono</label>
            <div class="input-group">
              <span class="input-group-addon" id="telefono-addon"><i class="glyphicon glyphicon-phone-alt"></i></span>
              <input type="text" class="form-control" placeholder="Teléfono" aria-describedby="telefono-addon" id="telefono" name="telefono" value="<?php echo set_value('telefono', isset($contacto->telefono)?$contacto->telefono:''); ?>">
            </div>
          </div>
          <div class="form-group">
            <label for="email">Dirección de e-Mail</label>
            <div class="input-group">
              <span class="input-group-addon" id="email-addon"><i class="glyphicon glyphicon-envelope"></i></span>
              <input type="text" class="form-control" placeholder="Email" aria-describedby="email-addon" id="email" name="email" value="<?php echo set_value('email', isset($contacto->email)?$contacto->email:''); ?>">
            </div>
          </div>
          <div class="form-group">
            <label for="direccion">Dirección</label>
            <div class="input-group">
              <span class="input-group-addon" id="direccion-addon"><i class="glyphicon glyphicon-home"></i></span>
              <input type="text" class="form-control" placeholder="Dirección" aria-describedby="direccion-addon" id="direccion" name="direccion" value="<?php echo set_value('direccion', isset($contacto->direccion)?$contacto->direccion:''); ?>">
            </div>
          </div>
          <div class="form-group col-md-offset-4">
            <div class="btn-group">
              <a href="<?=site_url("/")?>" class="btn btn-danger">Cancelar</a>
              <input type="submit" class="btn btn-success" value="<?=isset($contacto->id)?'Modificar':'Agregar'?>">
            </div>
          </div>
          <?php if(isset($contacto->id)) { ?>
            <input type="hidden" name="id" value="<?=$contacto->id?>">
          <?php } ?>
        </form>
      </div>
